import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Solution {

	private static boolean allTypesOk(Map<Integer, Boolean> typeContainer) {
		int totalNOOk = 0;
		for (Map.Entry<Integer, Boolean> entry : typeContainer.entrySet()) {
			if (entry.getValue())
				totalNOOk++;
		}
		if (totalNOOk == 1)
			return true;
		else
			return false;
	}

	public static void main(String[] args) {
		Map<Integer, Boolean> typeContainerMap;
		Scanner in = new Scanner(System.in);
		int q = in.nextInt();

		for (int a0 = 0; a0 < q; a0++) {
			typeContainerMap = new HashMap<Integer, Boolean>();
			int n = in.nextInt();
			int[][] M = new int[n][n];
			for (int M_i = 0; M_i < n; M_i++) {
				for (int M_j = 0; M_j < n; M_j++) {
					M[M_i][M_j] = in.nextInt();
				}
			}

			for (int i = 0; i < n; i++) {
				typeContainerMap.put(i, true);
			}

			long sumHorizontal;
			long sumVertical;
			Boolean flagFound = false;
			for (int i = 0; i < n && !allTypesOk(typeContainerMap); i++) {

				flagFound = false;
				for (int j = 0; j < n && !flagFound; j++) {
					sumHorizontal = 0;
					sumVertical = 0;
					if (typeContainerMap.get(j).equals(true)) {
						for (int r = 0; r < n; r++) {
							if (r != j)
								sumHorizontal += M[i][r];

							if ((n - r - 1) != i)
								sumVertical += M[n - r - 1][j];
						}
						if (sumHorizontal == sumVertical) {
							typeContainerMap.put(j, false);
							flagFound = true;

						}
					}
				}
			}
			if (allTypesOk(typeContainerMap))
				System.out.println("Possible");
			else
				System.out.println("Impossible");

		}
	}
}
